=====
Info:
=====
The script drills into a folder(s) with images and creates the corresponding KML file for opening with Google Earth. Placemarks are created from the coordinates and timestamps provided in the images' EXIF metadata.

-------------
Requirements:
-------------
Python 2.7.x, pip

-------------
Installation:
-------------
Clone or download the repository into a folder of your choice. Make sure you have the virtualenv installed so you can use it.
With wrapper::

    mkvirtualenv geotag-gallery

Without wrapper::

    virualenv env

Activate it:

With wrapper::

    workon geotag-gallery

Without wrapper::

    . ./env/bin/activate

Install requirements::

    pip install -r requirements.txt

Install corpora for textblob::

    python -m textblob.download_corpora

------
Usage:
------
::

    python geotag-gallery.py --folder=/path/to/folder/with/geotagged/images --language=hr

It will access the named folder and drill through the images and images in subfolders it encounters. When it's done collecting the data, it will create a KML file that can be opened with Google Earth and it will place the KML file in the named folder, too. The file should remain there since it's referencing the images relatively.
Tested on Ubuntu 18.04, as well as on Windows 10, WSL (Ubuntu 16.04), both with virtualenvwrapper and Python 2.7.x.
The parameter "language" is optional and can be ommited. It will default to english. It uses Keras ResNet50 at the moment to classify images and Textblob to translate the terms. It also uses nominatim.openstreetmap.org to reverse geocode the coordinates.

------
Notes:
------
Since the package is provided as is, just make sure you have the dependencies installed and it's running on Python 2.7.x.


-------
Future:
-------
Improve usage.
